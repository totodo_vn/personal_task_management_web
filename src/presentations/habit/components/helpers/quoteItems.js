export default [
  "Dành thời gian cho những điều tốt đẹp",
  "Bạn hoàn toàn có thể làm được điều này",
  "Bây giờ hoặc không bao giờ",
  "Mọi khoảnh khắc đều quan trọng",
  "Giấc mơ sẽ không bao giờ thành hiện thực trừ khi bạn bắt tay vào làm",
  "Làm giỏi hơn nói hay",
  "Cứ liều thử xem",
  "Hãy ưu tiên cho bản thân",
  "Hãy tin vào chính mình",
  "Làm theo những gì bạn cho là đúng, chứ không phải những gì dễ dàng"
];

/// "Good things take time",
/// "You can totally do this",
/// "Now or never",
/// "Every moment matter",
/// "Dream don't work unless you do",
/// "Well done is better than well said",
/// "Go for it",
/// "Make yourself a priority",
/// "Believe in yourself",
/// "Do what is right, not what is easy"
