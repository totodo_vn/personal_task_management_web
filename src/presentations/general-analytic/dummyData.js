export default {
  dummy_day: {
    completedTasks: { current: 10, last: 4 },
    completedRate: { current: 83.33, last: 33.33 },
    completionDistribution: null,
    categoryRanking: {
      columns: [
        { name: "Vacation Prep 🌴", tasks: 10, percent: 100 },
        { name: "Health ❤️", tasks: 9, percent: 90 },
        { name: "Learning Jappanese ⏰", tasks: 9, percent: 90 },
        { name: "House 😁", tasks: 8, percent: 80 }
      ]
    },
    completionRateDistribution: {
      completionRate: 83.33,
      columns: [
        { name: "overdue", tasks: 3, percent: 50 },
        { name: "onTime", tasks: 2, percent: 33 },
        { name: "undated", tasks: 0, percent: 0 },
        { name: "uncompleted", tasks: 1, percent: 16.7 }
      ]
    },
    classifiedCompletionStatistics: {
      byList: {
        completedTasks: 5,
        columns: [
          { name: "Inbox", color: "rgb(184, 37, 95)", tasks: 4 },
          { name: "Health ❤️", color: "rgb(175, 184, 59)", tasks: 1 }
        ]
      },
      byTag: {
        completedTasks: 10,
        columns: [
          { name: "Mobile", color: "rgb(250, 208, 0)", tasks: 1 },
          {
            name: "Frontend",
            color: "rgb(175, 56, 235)",
            tasks: 3
          },
          {
            name: "Backend",
            color: "rgb(106, 204, 188)",
            tasks: 6
          }
        ]
      }
    }
  },
  dummy_week: {
    completedTasks: { current: 10, last: 4 },
    completedRate: { current: 83.33, last: 33.33 },
    categoryRanking: {
      columns: [
        { name: "Vacation Prep 🌴", tasks: 10, percent: 100 },
        { name: "Health ❤️", tasks: 9, percent: 90 },
        { name: "Learning Jappanese ⏰", tasks: 9, percent: 90 },
        { name: "House 😁", tasks: 8, percent: 80 }
      ]
    },
    completionDistribution: {
      columns: [
        { name: "T2", tasks: 2 },
        { name: "T3", tasks: 5 },
        { name: "T4", tasks: 4 },
        { name: "T5", tasks: 12 },
        { name: "T6", tasks: 7 },
        { name: "T7", tasks: 8 },
        { name: "CN", tasks: 8 }
      ]
    },
    completionRateDistribution: {
      /* completionRate: 83.33, */
      columns: [
        { name: "overdue", tasks: 3, percent: 50 },
        { name: "onTime", tasks: 2, percent: 33 },
        { name: "undated", tasks: 0, percent: 0 },
        { name: "uncompleted", tasks: 1, percent: 16.7 }
      ]
    },
    classifiedCompletionStatistics: {
      byList: {
        completedTasks: 10,
        columns: [
          { name: "Inbox", color: "rgb(184, 37, 95)", tasks: 4 },
          { name: "Health ❤️", color: "rgb(175, 184, 59)", tasks: 1 }
        ]
      },
      byTag: {
        completedTasks: 10,
        columns: [
          { name: "Mobile", color: "rgb(250, 208, 0)", tasks: 1, percent: 10 },
          {
            name: "Frontend",
            color: "rgb(175, 56, 235)",
            tasks: 3
          },
          {
            name: "Backend",
            color: "rgb(106, 204, 188)",
            tasks: 6
          }
        ]
      }
    }
  },
  dummy_month: {
    completedTasks: { current: 40, last: 10 },
    completedRate: { current: 83.33, last: 33.33 },
    categoryRanking: {
      columns: [
        { name: "Inbox", tasks: 4, percent: 100 },
        { name: "Health ❤️", tasks: 9, percent: 90 },
        { name: "Learning Jappanese ⏰", tasks: 9, percent: 90 },
        { name: "House 😁", tasks: 8, percent: 80 }
      ]
    },
    completionDistribution: {
      columns: [
        { name: "ng7", tasks: 2 },
        { name: "ng14", tasks: 5 },
        { name: "ng21", tasks: 4 },
        { name: "ng28", tasks: 12 }
      ]
    },
    completionRateDistribution: {
      completionRate: 83.33,
      columns: [
        { name: "overdue", tasks: 30, percent: 30 },
        { name: "onTime", tasks: 10, percent: 10 },
        { name: "undated", tasks: 0, percent: 0 },
        { name: "uncompleted", tasks: 60, percent: 60 }
      ]
    },
    classifiedCompletionStatistics: {
      byList: {
        completedTasks: 40,
        columns: [
          {
            name: "Vacation Prep 🌴",
            color: "rgb(255, 153, 51)",
            tasks: 10
          },
          {
            name: "Health ❤️",
            color: "rgb(175, 184, 59)",
            tasks: 9
          },
          {
            name: "Learning Jappanese ⏰",
            color: "rgb(64, 115, 255)",
            tasks: 9
          },
          {
            name: "House 😁",
            color: "rgb(235, 150, 235)",
            tasks: 8
          },
          { name: "Inbox", color: "rgb(184, 37, 95)", tasks: 4, percent: 80 }
        ]
      },
      byTag: {
        completedTasks: 10,
        columns: [
          { name: "Mobile", color: "rgb(250, 208, 0)", tasks: 10, percent: 10 },
          {
            name: "Frontend",
            color: "rgb(175, 56, 235)",
            tasks: 10
          },
          {
            name: "Backend",
            color: "rgb(106, 204, 188)",
            tasks: 15
          },
          { name: "Không có thẻ", color: "grey", tasks: 5 }
        ]
      }
    }
  }
};
