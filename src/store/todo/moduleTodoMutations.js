/*=========================================================================================
  File Name: moduleTodoMutations.js
  Description: Todo Module Mutations
==========================================================================================*/

export default {
  SET_TODO_SEARCH_QUERY(state, query) {
    state.todoSearchQuery = query;
  },
  UPDATE_TODO_FILTER(state, filter) {
    state.todoFilter = filter;
  },

  // API
  SET_TASKS(state, tasks) {
    state.tasks = tasks;
  },
  SET_TAGS(state, tags) {
    state.taskTags = tags;
  },
  SET_PROJECTS(state, projects) {
    state.taskProjects = projects;
  },
  ADD_TASK(state, task) {
    state.tasks.unshift(task);
  },
  UPDATE_TASK(state, task) {
    const taskIndex = state.tasks.findIndex(t => t._id == task._id);
    Object.assign(state.tasks[taskIndex], task);
  },
  SET_TAG_SEARCH_QUERY(state, query) {
    state.tagSearchQuery = query;
  },
  SET_STATISTIC_DATA(state, statisticData) {
    state.statisticData = statisticData;
  }
};
