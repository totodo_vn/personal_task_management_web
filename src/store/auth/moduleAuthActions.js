/*=========================================================================================
  File Name: moduleAuthActions.js
  Description: Auth Module Actions
==========================================================================================*/

import jwt from "../../services/http/requests/auth/jwt/index.js";

import firebase from "firebase/app";
import "firebase/auth";
import router from "@/router";

export default {
  /* <GOOGLE LOGIN> */
  async loginWithGoogle({ commit, state }) {
    const provider = new firebase.auth.GoogleAuthProvider();

    try {
      var res = await firebase.auth().signInWithPopup(provider);

      if (res != null) {
        await _verifyGoogleLogin({
          accessToken: res.credential.accessToken,
          commit: commit
        });
      }
    } catch (error) {
      throw error;
    }
  },
  /* </GOOGLE LOGIN> */

  /* <FACEBOOK LOGIN> */
  async loginWithFacebook({ commit, state }) {
    const provider = new firebase.auth.FacebookAuthProvider();

    try {
      var res = await firebase.auth().signInWithPopup(provider);

      if (res != null) {
        await _verifyFacebookLogin({
          accessToken: res.credential.accessToken,
          commit: commit
        });
      }
    } catch (error) {
      throw error;
    }
  },
  /* </FACEBOOK LOGIN> */

  /* <LOGIN JWT> */
  async loginJWT({ commit }, payload) {
    try {
      var { result } = await jwt.login({
        email: payload.userDetails.email,
        password: payload.userDetails.password
      });

      if (result != null) {
        const { user, accessToken } = result;

        // Remember Me
        if (payload.checkbox_remember_me) {
          commit("SET_SAVED_ACCOUNT", JSON.stringify(payload.userDetails));
        }

        _goToMain({
          commit: commit,
          user: user,
          jwtToken: accessToken
        });
      }
    } catch (error) {
      throw error;
    }
  },
  /* </LOGIN JWT> */

  /* <REGISTER JWT> */
  async registerUserJWT({ commit }, payload) {
    const {
      displayName,
      email,
      password,
      confirmPassword
    } = payload.userDetails;

    if (password !== confirmPassword) {
      throw { message: "Mật khẩu không khớp. Vui lòng thử lại." };
    }

    try {
      var { message } = await jwt.registerUser({
        displayName: displayName,
        email: email,
        password: password
      });

      if (message != null) {
        // need to check email to activate account
        payload.notify({
          title: "Thành công",
          text: message,
          iconPack: "feather",
          icon: "icon-check",
          color: "success"
        });
      }

      // Redirect User
      router.push(router.currentRoute.query.to || "/pages/login");
    } catch (error) {
      throw error;
    }
  },
  /* </REGISTER JWT> */

  /* <REFRESH TOKEN> */
  fetchAccessToken() {
    return new Promise(resolve => {
      jwt.refreshToken().then(response => {
        resolve(response);
      });
    });
    // return localStorage.getItem("accessToken");
  }
  /* </REFRESH TOKEN> */
};

async function _verifyGoogleLogin({ accessToken, commit }) {
  try {
    var { result } = await jwt.verifyGoogleLogin({
      userAccessToken: accessToken
    });

    if (result != null) {
      var { user, jwtToken } = result;
      // Navigate user to Homepage
      router.push(router.currentRoute.query.to || "/");

      // Set accessToken
      localStorage.setItem("accessToken", jwtToken);

      // Update user details
      commit(
        "UPDATE_USER_INFO",
        { ...user, photoURL: user.avatar },
        { root: true }
      );

      // Set bearer token in axios
      commit("SET_BEARER", jwtToken);
    }
  } catch (error) {
    throw error;
  }
}

async function _verifyFacebookLogin({ accessToken, commit }) {
  try {
    var { result } = await jwt.verifyFacebookLogin({
      userAccessToken: accessToken
    });

    if (result != null) {
      var { user, jwtToken } = result;

      _goToMain({
        commit: commit,
        user: user,
        jwtToken: jwtToken
      });
    }
  } catch (error) {
    throw error;
  }
}

function _goToMain({ commit, user, jwtToken }) {
  // Navigate user to Homepage
  router.push(router.currentRoute.query.to || "/");

  // Set accessToken
  localStorage.setItem("accessToken", jwtToken);

  // Update user details
  commit(
    "UPDATE_USER_INFO",
    { ...user, photoURL: user.avatar },
    { root: true }
  );

  // Set bearer token in axios
  commit("SET_BEARER", jwtToken);
}
