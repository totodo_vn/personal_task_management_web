/*=========================================================================================
  File Name: moduleHabitActions.js
  Description: Habit Module Actions
==========================================================================================*/

import axios from "@/axios.js";
import HabitService from "../../services/HabitService";

export default {
  setHabitSearchQuery({ commit }, query) {
    commit("SET_HABIT_SEARCH_QUERY", query);
  },

  async fetchHabits({ commit }, payload) {
    try {
      const { result } = await HabitService.getList({ filter: payload.filter });

      const items = (result || []).map(item => ({
        ...item.habit
        // _id: item._id
      }));

      commit("SET_HABITS", items);
      return result;
    } catch (error) {
      console.log(error.message);
    }
    return [];
  },

  async checkin({ commit }, payload) {
    const { habitId, count, date, notify, callback } = payload;
    try {
      const { result, message } = await HabitService.checkin(habitId, {
        count,
        date
      });

      // tam thoi => nen sua du lieu tra ve o backend
      const temp = await HabitService.getDetail(habitId);

      commit("UPDATE_HABIT", temp.result);

      notify({
        title: "Thành công",
        text: message,
        iconPack: "feather",
        icon: "icon-check",
        color: "success"
      });

      if (callback) {
        callback();
      }
    } catch (error) {
      throw error;
    }
  },

  async fetchDiaries({ commit }, payload) {
    try {
      const { result } = await HabitService.getAllDiaries();

      commit("SET_DIARIES", result);
      return result;
    } catch (error) {
      console.log(error.message);
    }

    return [];
  },

  async addHabit({ commit }, payload) {
    const { habit, notify, callback } = payload;
    try {
      const { result, message } = await HabitService.create(habit);

      // tam thoi => nen sua du lieu tra ve o backend
      const temp = await HabitService.getDetail(result._id);

      commit("ADD_HABIT", Object.assign(temp.result, { id: result._id }));

      notify({
        title: "Thành công",
        text: message,
        iconPack: "feather",
        icon: "icon-check",
        color: "success"
      });

      if (callback) {
        callback();
      }
    } catch (error) {
      throw error;
    }
  },

  async updateHabit({ commit }, payload) {
    const { habit, notify, callback } = payload;
    try {
      const { result, message } = await HabitService.update(habit._id, habit);

      // tam thoi => nen sua du lieu tra ve o backend
      const temp = await HabitService.getDetail(result._id);

      commit("UPDATE_HABIT", temp.result);

      notify({
        title: "Thành công",
        text: message,
        iconPack: "feather",
        icon: "icon-check",
        color: "success"
      });

      if (callback) {
        callback();
      }
    } catch (error) {
      throw error;
    }
  },

  async deleteHabit({ commit }, payload) {
    const { habitId, notify } = payload;
    try {
      const { result, message } = await HabitService.delete(habitId);

      notify({
        title: "Thành công",
        text: message,
        iconPack: "feather",
        icon: "icon-check",
        color: "success"
      });

      const res = await HabitService.getList({ filter: payload.filter });

      const items = (res.result || []).map(item => ({
        ...item.habit
        // _id: item._id
      }));

      commit("SET_HABITS", items);
    } catch (error) {
      throw error;
    }
  },

  async addDiary({ commit }, payload) {
    const { habitId, diary, notify, callback } = payload;
    try {
      const { result, message } = await HabitService.createDiary(
        habitId,
        diary
      );

      // tam thoi => nen sua du lieu tra ve o backend
      // const temp = await HabitService.getDetailDiary(result._id);
      // commit("ADD_DIARY", Object.assign(temp.result, { id: result._id }));

      notify({
        title: "Thành công",
        text: message,
        iconPack: "feather",
        icon: "icon-check",
        color: "success"
      });

      if (callback) {
        callback();
      }
    } catch (error) {
      throw error;
    }
  },

  async deleteDiary({ commit }, payload) {
    const { diaryId, notify } = payload;
    try {
      const { result, message } = await HabitService.deleteDiary(diaryId);

      notify({
        title: "Thành công",
        text: message,
        iconPack: "feather",
        icon: "icon-check",
        color: "success"
      });

      const res = await HabitService.getAllDiaries();

      commit("SET_DIARIES", res.result);
    } catch (error) {
      throw error;
    }
  },

  setDiarySearchQuery({ commit }, query) {
    commit("SET_DIARY_SEARCH_QUERY", query);
  }
};
