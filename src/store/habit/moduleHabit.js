/*=========================================================================================
  File Name: moduleHabit.js
  Description: Habit Module
==========================================================================================*/

import state from "./moduleHabitState.js";
import mutations from "./moduleHabitMutations.js";
import actions from "./moduleHabitActions.js";
import getters from "./moduleHabitGetters.js";

export default {
  namespaced: true,
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
};
