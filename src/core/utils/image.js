import checkinIconItems from "../../constants/checkinIconItems";
import checkinImageItems from "../../constants/checkinImageItems";

export function getAssetImage(data, type = "checkin-icon") {
  if (data.toString().includes("http")) {
    return data;
  } else {
    const i = parseInt(data);
    if (type === "checkin-icon") return checkinIconItems[i - 1];
    return checkinImageItems[i - 1];
  }
}
