import { addSpaceCamelCase } from "./string";

export function convertSelectList(target) {
  const result = [];

  for (const k in target) {
    result.push({
      label: target[k].label || addSpaceCamelCase(k),
      id: target[k].id != null ? target[k].id : target[k],
      ...target[k]
    });
  }

  return result;
}

export function groupByKey(list, key) {
  if (!list) {
    return [];
  }

  return list.reduce(function(r, a) {
    r[a[key]] = r[a[key]] || [];
    r[a[key]].push(a);

    return r;
  }, {});
}

export function groupBy(list, ev) {
  if (!list) {
    return [];
  }

  return list.reduce(function(r, a) {
    const key = ev(a);
    r[key] = r[key] || [];
    r[key].push(a);

    return r;
  }, {});
}
