import * as RequestFactory from "./RequestFactory";

export default {
  get(url, params) {
    return RequestFactory.get({ url, params });
  },

  put(url, data, params) {
    return RequestFactory.put({ url, data, params });
  },

  patch(url, data, params) {
    return RequestFactory.patch({ url, data, params });
  },

  post(url, data, params) {
    return RequestFactory.post({ url, data, params });
  },

  postFile(url, data, params) {
    return RequestFactory.postFile({ url, data, params });
  },

  delete(url, params) {
    return RequestFactory._delete({ url, params });
  }
};
