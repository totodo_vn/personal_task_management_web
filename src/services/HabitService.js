import BaseService from "../core/common/BaseService";

export default {
  getList() {
    return BaseService.get("/habits/by-user", {});
  },
  getDetail(habitId) {
    return BaseService.get(`/habits/${habitId}`, {});
  },
  create(data) {
    return BaseService.post("/habits", data, {});
  },
  update(habitId, data) {
    return BaseService.patch(`/habits/${habitId}`, data, {});
  },
  delete(habitId) {
    return BaseService.delete(`/habits/${habitId}`, {});
  },
  checkin(habitId, data) {
    return BaseService.put(`/habits/${habitId}/check-in`, data, {});
  },
  finish(habitId) {
    return BaseService.put(`/habits/${habitId}/finish`, {}, {});
  },
  createDiary(habitId, data) {
    const _data = { ...data, files: data.images };
    return BaseService.postFile(`/habits/${habitId}/add-diary`, _data, {});
  },
  getAllDiaries() {
    return BaseService.get(`/diaries`, {});
  },
  getDiariesByHabit(habitId) {
    return BaseService.get(`/habits/${habitId}/diaries`, {});
  },
  deleteDiary(diaryId) {
    return BaseService.delete(`/diaries/${diaryId}`, {});
  }
};
