import BaseService from "../core/common/BaseService";

export default {
  getList(projectId) {
    return BaseService.get(`/projects/${projectId}/sections`, {});
  },
  getDetail(projectId, sectionId) {
    return BaseService.get(`/projects/${projectId}/sections/${sectionId}`, {});
  },
  create(projectId, data) {
    return BaseService.post(`/projects/${projectId}/sections`, data, {});
  },
  update(projectId, sectionId, data) {
    return BaseService.put(
      `/projects/${projectId}/sections/${sectionId}`,
      data,
      {}
    );
  }
};
